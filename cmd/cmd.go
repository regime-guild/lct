package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	"gitlab.com/hierarchy-guild/lct/common/errors"
	"gitlab.com/hierarchy-guild/lct/actions/guild"
	"gitlab.com/hierarchy-guild/lct/actions/item"
	"gitlab.com/hierarchy-guild/lct/actions/player"
)

var (
	config string
	level string
	itemsFile string
	guildFile string
	gitbase string
	clonebase string
	resources string
	tracking string
	username string
	password string
)

var name = "lct"
var validConfigFilenames = []string{"yaml", "yml"}

func Execute() {
	cFunc := func(cmd *cobra.Command, args []string) error {
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}


		return nil
	}

	cmd := cli.NewCommand(name, "Loot Council Tool for managing of guild look tracking",
						  "Loot Council Tool for managing of guild look tracking", cFunc)

	cmd.PersistentFlags().StringVar(&level, "level", "info", "Set logging level")
	cmd.PersistentFlags().StringVar(&config, "config", "lct", "Set config file")
	cmd.PersistentFlags().SetAnnotation("config", cobra.BashCompFilenameExt, validConfigFilenames)
	
	cmd.PersistentFlags().StringVar(&gitbase, "gitbase", "https://gitlab.com/hierarchy-guild", "Git repo base")
	cmd.PersistentFlags().StringVar(&clonebase, "clonebase", ".lct/clones", "Base path for all git clones")
	cmd.PersistentFlags().StringVar(&resources, "resources", "wow-resources", "Git repo for resources")
	cmd.PersistentFlags().StringVar(&tracking, "tracking", "loot-tracking", "Git repo for loot council tracking")
	cmd.PersistentFlags().StringVar(&username, "username", "", "Git username")
	cmd.PersistentFlags().StringVar(&password, "password", "", "Git passowrd")
	viper.BindPFlag("gitbase", cmd.PersistentFlags().Lookup("gitbase"))
	viper.BindPFlag("clonebase", cmd.PersistentFlags().Lookup("clonebase"))
	viper.BindPFlag("resources", cmd.PersistentFlags().Lookup("resources"))
	viper.BindPFlag("tracking", cmd.PersistentFlags().Lookup("tracking"))
	viper.BindPFlag("username", cmd.PersistentFlags().Lookup("username"))
	viper.BindPFlag("password", cmd.PersistentFlags().Lookup("password"))
	
	cmd.PersistentFlags().StringVar(&itemsFile, "items", "items.json", "Set items file")
	cmd.PersistentFlags().StringVar(&guildFile, "guild", "guild.json", "Set guild file")
	viper.BindPFlag("items", cmd.PersistentFlags().Lookup("items"))
	viper.BindPFlag("guild", cmd.PersistentFlags().Lookup("guild"))

	cmd.AddCommand(guild.GetCommand(name))
	cmd.AddCommand(item.GetCommand(name))
	cmd.AddCommand(player.GetCommand(name))

	cmd.MinimumNArgs(1)

	c, err := cmd.ExecuteC()
	if err != nil {
		if errors.IsUserError(err) {
			c.Println(err)
			c.Println(c.UsageString())
		}
		os.Exit(-1)
	}
}