package main

import (

	"gitlab.com/hierarchy-guild/lct/cmd"
	//"gitlab.com/hierarchy-guild/lct/common"
	//"gitlab.com/hierarchy-guild/lct/common/types/item"
	//"gitlab.com/hierarchy-guild/lct/common/types/itemdb"
	//"gitlab.com/hierarchy-guild/lct/common/types/loot"
	//"gitlab.com/hierarchy-guild/lct/common/types/player"
	//"gitlab.com/hierarchy-guild/lct/common/types/guild"
)


func main() {
	cmd.Execute()	
	// itemdb, err := itemdb.LoadItemDB("items.json")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }

	// guild, err := guild.LoadGuild("guild.json")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }
	
	// it := os.Args[1]
	// common.PrettyPrint(guild.GetPlayer(it))

	// csvfile, err := os.Open("rcl.csv")
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// r := csv.NewReader(csvfile)

	// c := 0
	// for {
	// 	record, err := r.Read()
	// 	if err == io.EOF {
	// 		break
	// 	}
	// 	if err != nil {
	// 		fmt.Println(err)
	// 		break
	// 	}
	// 	if !Contains(record[7], "Offspec") &&
	// 		!Contains(record[7], "PVP") &&
	// 		!Contains(record[7], "Offline") &&
	// 		!Contains(record[7], "Pass") &&
	// 		!Contains(record[7], "Awarded") &&
	// 		!Contains(record[7], "Button4") &&
	// 		!Contains(record[7], "wait") &&
	// 		!Contains(record[7], "Autopass") &&
	// 		!Contains(record[7], "Personal") {
	// 		c = c + 1

	// 		itm := ItemIDMap[record[5]]
	// 		fmt.Printf("R: %s, %s, %s, %s, %s\n",
	// 			strings.Split(record[0], "-")[0],
	// 			strings.ReplaceAll(record[1], "/", "-"),
	// 			itm.Name,
	// 			itm.ID,
	// 			record[7])
	// 	}
	// }
	// fmt.Printf("Items found in csv: %d\n", c)
}

/*
CLI

-> guild
-> player
-> items

guild -> parse (from rcl csv) <filename>
guild -> load (pull from git)
guild -> update (push to git)

player -> manage -> add <name> <class> <spec>
                 -> delete <name>
player -> get <name>
player -> stier <name>
player -> loot -> add <name> <item> <date>
			   -> remove <name> <item>
player -> compare <name1> ... <nameN>

item -> search <item>
item -> stier
*/
