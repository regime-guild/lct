#!/bin/bash

set -x

releases=()
for release in `ls build/*.release`; do
    releases+=(`cat $release`)
done
releases=$(printf ",%s" "${releases[@]}")
releases=${releases:1}

payload="{\"name\":\"${CI_COMMIT_TAG}\",\"tag_name\":\"${CI_COMMIT_TAG}\",\"description\":\"${CI_COMMIT_TAG}\",\"milestones\":[\"${CI_COMMIT_TAG}\"],\"assets\":{\"links\":[${releases}]}}"
echo $payload | jq
payload="'$payload'"

curl --verbose --header "Content-Type: application/json" --header "PRIVATE-TOKEN: ${GL_TOKEN}" --data ${payload} https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases