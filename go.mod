module gitlab.com/hierarchy-guild/lct

go 1.13

require (
	github.com/bleggett/coverfail v0.0.0-20200110191057-ca75295c3030 // indirect
	github.com/go-openapi/strfmt v0.19.4 // indirect
	github.com/hhatto/gocloc v0.3.1 // indirect
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/mattn/go-runewidth v0.0.7 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/jwalterweatherman v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.3.2
	github.com/thoas/go-funk v0.5.0
	gopkg.in/src-d/go-billy.v4 v4.3.2
	gopkg.in/src-d/go-git.v4 v4.13.1
)
