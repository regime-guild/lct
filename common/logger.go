package common

import (
	// "log"
	
	jww "github.com/spf13/jwalterweatherman"
)

func InitLogger(level string) error {
	var threshold jww.Threshold

	switch lvl := level; lvl {
	case "info":
			threshold = jww.LevelInfo
	case "debug":
		threshold = jww.LevelDebug
	case "warn":
		threshold = jww.LevelWarn
	default:
		threshold = jww.LevelError
	}

	jww.SetLogThreshold(threshold)
	jww.SetStdoutThreshold(threshold)
	jww.SetPrefix("")
	jww.SetFlags(0)

	return nil
}