package common

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func LoadGlobals(prefix, config string, defaults ...map[string]interface{}) error {
	viper.AutomaticEnv()
	viper.SetEnvPrefix(prefix)
	viper.SetConfigType("yaml")
	viper.SetConfigName(config)
	viper.AddConfigPath("/etc/" + prefix)
	viper.AddConfigPath("$HOME/." + prefix)
	viper.AddConfigPath(".")
	
	err := viper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigParseError); ok {
			return err
		}
		return fmt.Errorf("Unable to load config file. (%s)\n", err)
	}
	
	for _, v := range defaults {
		setDefaults(v)
	}

	return nil
}

func setDefaults(defaults map[string]interface{}) {
	for k, v := range defaults {
		viper.SetDefault(k, v)
	}
}

func fromFlag(flags *pflag.FlagSet, key string) {
	flag := flags.Lookup(key)
	if flag != nil {
		if flag.Changed {
			viper.Set(key, flag.Value.String())
		}
	}
}

func initFlags(cmd *cobra.Command) {
	persFlagKeys := []string{"verbose"}
	flagKeys := []string{}

	for _, key := range persFlagKeys {
		fromFlag(cmd.PersistentFlags(), key)
	}

	for _, key := range flagKeys {
		fromFlag(cmd.Flags(), key)
	}
}


func InitConfig(block, cfgFile, level string, subCmdVs ...*cobra.Command) error {
	err := LoadGlobals(block, cfgFile)
	if err != nil {
		return err
	}

	for _, cmdV := range subCmdVs {
		initFlags(cmdV)
	}

	err = InitLogger(level)
	if err != nil {
		return err
	}

	return nil
}