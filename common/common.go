package common

import (
	"fmt"
	"encoding/json"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"
)

const (
	DATEFMT = "01-02-06"
)


func CheckIfError(err error) {
	if err == nil {
		return
	}

	jww.ERROR.Println(err)
	os.Exit(1)
}

// Case insensitive version of strings.Contains
func Contains(str string, sub string) bool {
	return strings.Contains(strings.ToLower(str), strings.ToLower(sub))
}


// Print out object as well structured and formated JSON
func PrettyPrint(v interface{}) (string, error) {
	b, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		return "", err
	}
	return string(b), nil
}

// Wraper around opening a file, reading Byte array, and unmarshalling to json
func GetJson(file string, v interface{}) error {
	file, err := GetPath(file)
	CheckIfError(err)

	jww.DEBUG.Printf("Opening file: %s", file)
	handler, err := os.Open(file)
	if err != nil {
		jww.FATAL.Printf("Failed to open file: %s", file)
		jww.FATAL.Println(err)
		return err
	}
	defer handler.Close()

	jww.DEBUG.Printf("Reading contents of file: %s", file)
	byteValue, err := ioutil.ReadAll(handler)
	if err != nil {
		jww.FATAL.Printf("Failed to read contents file: %s", file)
		jww.FATAL.Println(err)
		return err
	}

	jww.DEBUG.Printf("Unmarshaling to JSON contents read from file: %s", file)
	err = json.Unmarshal(byteValue, &v)
	if err != nil {
		jww.FATAL.Printf("Failed to unmarshal contenst read from file: %s", file)
		jww.FATAL.Println(err)
		return err
	}

	return nil
}

func LoadRepo(repo string) (string, error) {
	base := viper.GetString("gitbase")
	clone := viper.GetString("clonebase")
	username := viper.GetString("username")
	password := viper.GetString("password")

	return CloneRepo(base, clone, repo, username, password)
}

func GetPath(file string) (string, error) {
	clone := viper.GetString("clonebase")

	usr, _ := user.Current()
	clone = usr.HomeDir + "/" + clone

	clone = filepath.FromSlash(clone)
	_, err := os.Stat(clone)
	if os.IsNotExist(err) {
		jww.DEBUG.Println(clone + " doest not exist! Please Load Resources and Tracking first")
		return "", err
	}

	var found string

	filepath.Walk(clone, func(path string, f os.FileInfo, _ error) error {
		if err != nil {
			return err
		}

		if Contains(path, file) {
			found = path
		}
		return nil
	})

	if found == "" {
		return "", fmt.Errorf("File not found in search path")
	}

	return filepath.FromSlash(found), nil
}