package common

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"time"

	jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"
	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
)

func CloneRepo(base string, clone string, repo string, username string, password string) (string, error) {
	jww.DEBUG.Println("Check/Create clone path")
	usr, _ := user.Current()
	clone = usr.HomeDir + "/" + clone
	_, err := os.Stat(filepath.FromSlash(clone))
	if os.IsNotExist(err) {
		jww.DEBUG.Println(clone + " doest not exist!")
		os.MkdirAll(clone, os.ModePerm)
	}

	jww.DEBUG.Println("Check if repo is already cloned")
	
	path := clone + "/" + repo
	giturl := base + "/" + repo
	path = filepath.FromSlash(path)
	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		jww.DEBUG.Println("Clone repo: " + giturl)
		_, err := git.PlainClone(path, false, &git.CloneOptions{
			Auth: &http.BasicAuth{
				Username: username,
				Password: password,
			},
			URL: giturl,
			Progress: os.Stdout,
		})
		if err != nil {
			return "", err
		}
	}

	jww.DEBUG.Println("Return path to repo")
	return path, nil
}

type GitRepo struct {
	Repo *git.Repository
}

func (gr *GitRepo) Worktree() *git.Worktree {
	wt, err := gr.Repo.Worktree()
	if err != nil {
		jww.ERROR.Println("Cannot get worktree.")
		CheckIfError(err)
	}

	return wt
}

func (gr *GitRepo) Status() git.Status {
	w := gr.Worktree()
	s, err := w.Status()
	CheckIfError(err)
	
	return s
}

func (gr *GitRepo) IsClean() bool {
	status := gr.Status()
	return status.IsClean()
}

func (gr *GitRepo) Add(path string) {
	w := gr.Worktree()
	path = filepath.FromSlash(path)
	_, err := w.Add(path)
	CheckIfError(err)
}

func (gr *GitRepo) BranchCreate(branch string) {
	head, err := gr.Repo.Head()
	CheckIfError(err)

	ref := plumbing.NewHashReference(plumbing.NewBranchReferenceName(branch), head.Hash())

	err = gr.Repo.Storer.SetReference(ref)
	CheckIfError(err)
}

func (gr *GitRepo) BranchDelete(branch string)  {
	err := gr.Repo.Storer.RemoveReference(plumbing.NewBranchReferenceName(branch))
	CheckIfError(err)
}

func (gr *GitRepo) Checkout(branch string) {
	w := gr.Worktree()
	co := &git.CheckoutOptions{}
	co.Branch = plumbing.NewBranchReferenceName(branch)
	err := w.Checkout(co)
	CheckIfError(err)
}

func (gr *GitRepo) Clean() {
	w := gr.Worktree()
	err := w.Clean(&git.CleanOptions{})
	CheckIfError(err)
}

func (gr *GitRepo) Commit(msg string, date time.Time) {
	w := gr.Worktree()
	auth := &object.Signature {
		Name: "LCT",
		Email: "",
		When: date,
	}
	_, err := w.Commit(msg, &git.CommitOptions{All: true, Author: auth})
	CheckIfError(err)
}

func (gr *GitRepo) Pull() {
	if !gr.IsClean() {
		CheckIfError(fmt.Errorf("You currently have changes that have not been pushed to remote."))
	}

	username := viper.GetString("username")
	password := viper.GetString("password")

	w := gr.Worktree()
	err := w.Pull(&git.PullOptions{
		Auth: &http.BasicAuth{
			Username: username,
			Password: password,
		},
	})
	if  err != nil {
		if err.Error() == git.NoErrAlreadyUpToDate.Error() {
			jww.INFO.Println("Repoistory already in sync with remote repository.")
			return
		}
		CheckIfError(err)
	}

	jww.INFO.Println("Repository sync'ed with remote repository")
	return 
}

func (gr *GitRepo) Push() {
	if !gr.IsClean() {
		CheckIfError(fmt.Errorf("You currently have changes that have not been pushed to remote."))
	}

	username := viper.GetString("username")
	password := viper.GetString("password")

	err := gr.Repo.Push(&git.PushOptions{
		Auth: &http.BasicAuth{
			Username: username,
			Password: password,
		},
	})
	CheckIfError(err)
}

func (gr *GitRepo) Reset() {
	w := gr.Worktree()
	err := w.Reset(&git.ResetOptions{Mode: git.HardReset})
	CheckIfError(err)
}

func GetRepo(path string) (*GitRepo, error) {
	path = filepath.FromSlash(path)
	r, err := git.PlainOpen(path)
	if err != nil {
		return nil, err
	}

	return &GitRepo{
		Repo: r,
	}, nil
}
