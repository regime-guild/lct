package player

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/jedib0t/go-pretty/table"
	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/types/loot"
)

type Player struct {
	Name  string
	Class string
	Spec  string
	Loot  loot.LootSlice
}

func (p *Player) GetLoot() []loot.Loot {
	return p.Loot
}

func (p *Player) AddLoot(loot *loot.Loot) {
	found := false 
	for _, v := range p.Loot {
		if v.Item.Name == loot.Item.Name && v.Date == loot.Date{
			found = true
		}
	}

	if !found {
		p.Loot = append(p.Loot, *loot)
	}
	sort.Sort(p.Loot)
}

func (p *Player) ToJson() (string, error) {
	s, err := common.PrettyPrint(p)
	if err != nil {
		return "", err
	}

	return s, err
}

func (p *Player) Display(md bool) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendRows([]table.Row{
		{"Name", p.Name},
		{"Class", p.Class},
		{"Spec", p.Spec},
		{""},
	})

	var trs []table.Row
	maxcol := 0
	for k, v := range p.Loot {
		if len(v.Item.Name) > maxcol {
			maxcol = len(v.Item.Name)
		}
		tr := table.Row{
			"",
			fmt.Sprintf("%d.", k+1),
			v.Item.Name,
			v.Date.Format(common.DATEFMT),
		}
		trs = append(trs, tr)
	}
	t.AppendRows([]table.Row{
		{"-----", "Loot", strings.Repeat("-", maxcol), "--------"},
		{"","","Item:","Date:"},
		{""},
	})
	t.AppendRows(trs)
	t.AppendRows([]table.Row{
		{""},
		{"","", "Number of Items:", len(p.Loot)},
	})

	if len(p.Loot) > 0 {
		last := p.Loot[len(p.Loot)-1]
		t.AppendRows([]table.Row{
			{"", "", "Last Loot Date:", last.Date.Format(common.DATEFMT)},
		})
	}

	if md {
		t.RenderMarkdown()
		return
	}

	t.Render()
}

func (p *Player) UnmarshalJSON(data []byte) error {
	type Alias Player
	aux := &struct {
		*Alias
	}{
		Alias: (*Alias)(p),
	}

	err := json.Unmarshal(data, &aux)
	if err != nil {
		return err
	}

	sort.Sort(p.Loot)

	return nil
}


func Compare(players []*Player, md bool) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)


	for i := 1; i <= len(players) / 2; i++ {
		var largest *Player
		var smallest *Player

		if len(players[(i*2)-2].Loot) >= len(players[(i*2)-1].Loot) {
			largest = players[(i*2)-2]
			smallest = players[(i*2)-1]
		} else {
			largest = players[(i*2)-1]
			smallest = players[(i*2)-2]
		}

		jww.DEBUG.Printf("Largest: %s with: %d", largest.Name, len(largest.Loot))
		jww.DEBUG.Printf("Smallest: %s with: %d", smallest.Name, len(smallest.Loot))

		t.AppendRows([]table.Row{
			{"Name", largest.Name, "", "", "", "Name", smallest.Name},
			{"Class", largest.Class, "", "", "", "Class", smallest.Class},
			{"Spec", largest.Spec, "", "", "", "Spec", smallest.Spec},
			{""},
		})

		var trs []table.Row
		maxcol1 := 0
		maxcol2 := 0

		for k, _ := range largest.Loot {
			if len(largest.Loot[k].Item.Name) > maxcol1 {
				maxcol1 = len(largest.Loot[k].Item.Name)
			}

			var tr table.Row
			if (k+1 <= len(smallest.Loot)) {
				if len(smallest.Loot[k].Item.Name) > maxcol2 {
					maxcol2 = len(smallest.Loot[k].Item.Name)
				}

				tr = table.Row{
					"",
					fmt.Sprintf("%d.", k+1),
					largest.Loot[k].Item.Name,
					largest.Loot[k].Date.Format(common.DATEFMT),
					"",
					fmt.Sprintf("%d.", k+1),
					smallest.Loot[k].Item.Name,
					smallest.Loot[k].Date.Format(common.DATEFMT),
				}
			} else {
				tr = table.Row{
					"",
					fmt.Sprintf("%d.", k+1),
					largest.Loot[k].Item.Name,
					largest.Loot[k].Date.Format(common.DATEFMT),
					"",
					"",
					"",
					"",
				}
			}
			
			trs = append(trs, tr)
		}

		t.AppendRows(trs)
		t.AppendRows([]table.Row{
			{""},
			{"","", "Number of Items:", len(largest.Loot), "", "", "", "Number of Items:", len(smallest.Loot)},
		})

		if len(largest.Loot) > 0 {
			lastLG := largest.Loot[len(largest.Loot)-1]
			var lastSM loot.Loot
			if len(smallest.Loot) > 0 {
				lastSM = smallest.Loot[len(smallest.Loot)-1]
			}

			t.AppendRows([]table.Row{
				{"", "", "Last Loot Date:", lastLG.Date.Format(common.DATEFMT), "", "", "", "Last Loot Date:", lastSM.Date.Format(common.DATEFMT)},
				{""},{""},
			})
		}
	}

	if len(players) %2 == 1 {
		t.AppendRows([]table.Row{
			{"Name", players[len(players)-1].Name, "", "", "", "", ""},
			{"Class", players[len(players)-1].Class, "", "", "", "", ""},
			{"Spec", players[len(players)-1].Spec, "", "", "", "", ""},
			{""},
		})

		var trs []table.Row
		maxcol1 := 0

		for k, _ := range players[len(players)-1].Loot {
			if len(players[len(players)-1].Loot[k].Item.Name) > maxcol1 {
				maxcol1 = len(players[len(players)-1].Loot[k].Item.Name)
			}
			
			tr := table.Row{
				"",
				fmt.Sprintf("%d.", k+1),
				players[len(players)-1].Loot[k].Item.Name,
				players[len(players)-1].Loot[k].Date.Format(common.DATEFMT),
				"",
				"",
				"",
				"",
			}
			trs = append(trs, tr)
		}

		t.AppendRows(trs)
		t.AppendRows([]table.Row{
			{""},
			{"","", "Number of Items:", len(players[len(players)-1].Loot), "", "", "", "", ""},
		})

		if len(players[len(players)-1].Loot) > 0 {
			lastLG := players[len(players)-1].Loot[len(players[len(players)-1].Loot)-1]
			
			t.AppendRow(table.Row{"", "", "Last Loot Date:", lastLG.Date.Format(common.DATEFMT), "", "", "", "", ""})
		}
	}
	

	if md {
		t.RenderMarkdown()
		return
	}

	t.Render()
}




func NewPlayer(name string, class string, spec string) *Player {
	return &Player{
		Name:  name,
		Class: class,
		Spec:  spec,
	}
}