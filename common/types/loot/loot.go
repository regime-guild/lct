package loot

import (
	"encoding/json"
	"time"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/types/item"
)

type Loot struct {
	Item item.Item
	Date time.Time
}

func (l *Loot) MarshalJSON() ([]byte, error) {
	type Alias Loot
	return json.Marshal(&struct {
		Date string `json: "date"`
		*Alias
	}{
		Date: l.Date.Format(common.DATEFMT),
		Alias: (*Alias)(l),
	})
}

func (l *Loot) UnmarshalJSON(data []byte) error {
	type Alias Loot
	aux := &struct {
		Date string `json: "date"`
		*Alias
	}{
		Alias: (*Alias)(l),
	}

	err := json.Unmarshal(data, &aux)
	if err != nil {
		return err
	}

	l.Date, err = time.Parse(common.DATEFMT, aux.Date)
	if err != nil {
		return err
	}

	return nil
}

func NewLoot(item item.Item, d time.Time) *Loot {
	return &Loot{
		item,
		d,
	}
}