package loot

type LootSlice []Loot

func (l LootSlice) Less(i,j int) bool {
	return l[i].Date.Before(l[j].Date)
}

func (l LootSlice) Swap(i, j int) {
	l[i].Date, l[j].Date = l[j].Date, l[i].Date
}

func (l LootSlice) Len() int {
	return len(l)
}