package guild

import (
	"io/ioutil"
	"os"
	"strings"

	jww "github.com/spf13/jwalterweatherman"
	"github.com/thoas/go-funk"
	
	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/types/player"
)

type Guild struct {
	Name    string
	Server  string
	Faction string
	Players []player.Player
}

func (g *Guild) AddPlayer(p player.Player) {
	g.Players = append(g.Players, p)
}

func (g *Guild) GetPlayer(name string) *player.Player {
	playerNameMap := funk.ToMap(g.Players, "Name").(map[string]player.Player)

	name = strings.Title(strings.ToLower(name))
	if player, ok := playerNameMap[name]; ok {
		return &player
	}
	return nil
}

func (g *Guild) UpdatePlayer(p player.Player) {
	for i := len(g.Players) - 1; i >= 0; i-- {
		if g.Players[i].Name == p.Name {
			g.Players = append(g.Players[:i], g.Players[i+1:]...)
		}
	}
	g.AddPlayer(p)
}

func (g *Guild) UpdateGuild(file string) *Guild {
	tmp, err := ioutil.TempFile("", "guild")
	common.CheckIfError(err)

	defer tmp.Close()
	defer os.Remove(tmp.Name())

	cur, err := common.GetPath(file)
	common.CheckIfError(err)

	out, err := common.PrettyPrint(LoadGuild(cur))
	common.CheckIfError(err)

	_, err = tmp.WriteString(out)
	common.CheckIfError(err)
	// err = tmp.Close()
	// common.CheckIfError(err)

	tmp.Sync()

	gf, err := os.OpenFile(cur, os.O_RDWR, 0644)
	common.CheckIfError(err)
	defer gf.Close()

	out, err = common.PrettyPrint(g)
	common.CheckIfError(err)
	
	_, err = gf.WriteString(out)
	common.CheckIfError(err)

	gf.Sync()

	return LoadGuild(cur)
}

// Load guild from json and return object
func LoadGuild(file string) *Guild {
	jww.DEBUG.Printf("Requesting Guild JSON from file: %s", file)
	var g Guild
	err := common.GetJson(file, &g)
	common.CheckIfError(err)
	return &g
}