package itemdb

import (
	"os"
	"regexp"
	"sort"
	"strconv"
	"sync"

	"github.com/jedib0t/go-pretty/table"
	jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"
	"github.com/thoas/go-funk"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/types/item"
)

type ItemDB struct {
	Items []item.Item
}

// Return usable ID map from Items
func (i *ItemDB) ID() map[string]item.Item {
	return funk.ToMap(i.Items, "ID").(map[string]item.Item)
}

// Return usable Name map from Items
func (i *ItemDB) Name() map[string]item.Item {
	return funk.ToMap(i.Items, "Name").(map[string]item.Item)
}

func (i *ItemDB) Search(value string) []item.Item {
	var items []item.Item

	var searchCheck = regexp.MustCompile("^[0-9]*$")
	if searchCheck.MatchString(value)  {
		for _, v := range i.Items {
			if common.Contains(v.ID, value) {
				items = append(items, v)
			}
		}
	} else {
		jww.DEBUG.Println("Searching itemsdb by Name")
		for _, v := range i.Items {
			if common.Contains(v.Name, value) {
				items = append(items, v)
			}
		}
	}

	return items
}

func Display(items []item.Item) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"ID", "Item"})
	
	var trs []table.Row

	for _, v := range items {
		tr := table.Row{v.ID, v.Name}
		trs = append(trs, tr)
	}
	
	t.AppendRows(trs)
	t.AppendFooter(table.Row{"Count", len(trs)})
	t.Render()
}

func DedupItems(items []item.Item) []item.Item {
	keys := make(map[string]bool)
	clean := []item.Item{}

	jww.DEBUG.Println("Dedup'ing list of items.")
	for _, entry := range items {
		if _, value := keys[entry.ID]; !value {
			keys[entry.ID] = true
			clean = append(clean, entry)
		} else {
		}
	}

	return clean
}

// Load itemdb from json and return object
func LoadItemDB(file string) (*ItemDB, error) {
	jww.DEBUG.Printf("Requesting ItemDB JSON from file: %s", file)
	var itemdb ItemDB
	err := common.GetJson(file, &itemdb)
	if err != nil {
		return nil, err
	}

	sort.Slice(itemdb.Items, func(i, j int) bool {
		k1, _ := strconv.Atoi(itemdb.Items[i].ID)
		k2, _ := strconv.Atoi(itemdb.Items[j].ID)
		return k1 < k2
	})

	itemdb.Items = DedupItems(itemdb.Items)

	return &itemdb, nil
}


var instance *ItemDB
var once sync.Once

func GetInstance() *ItemDB {
	once.Do(func() {
		items := viper.GetString("items")
		itemsFile, err := common.GetPath(items)
		common.CheckIfError(err)
		instance, err = LoadItemDB(itemsFile)
		common.CheckIfError(err)
	})

	return instance
}