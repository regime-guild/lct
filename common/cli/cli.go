package cli

import (
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

type CommandFunc func(cmg *cobra.Command, args []string) error
type NormFunc func(f *pflag.FlagSet, name string) pflag.NormalizedName

type Command struct {
	cmd *cobra.Command
}

func NewCommand(name, short, long string, cFunc CommandFunc) *Command {
	return &Command{
		cmd: &cobra.Command{
			Use: name,
			Short: short,
			Long: long,
			RunE: cFunc,
			SilenceUsage: true,
		},
	}
}

func (c *Command) GetCommand() *cobra.Command {
	return c.cmd
}

func (c *Command) Flags() *pflag.FlagSet {
	return c.cmd.Flags()
}

func (c *Command) PersistentFlags() *pflag.FlagSet {
	return c.cmd.PersistentFlags()
}

func (c *Command) AddCommand(cmd *cobra.Command) {
	c.cmd.AddCommand(cmd)
}

func (c *Command) ExecuteC() (*cobra.Command, error) {
	return c.cmd.ExecuteC()
}

func (c *Command) SetGlobalNormalizationFunc(f NormFunc) {
	c.cmd.SetGlobalNormalizationFunc(f)
}

func (c *Command) MinimumNArgs(count int) {
	c.cmd.Args = cobra.MinimumNArgs(count)
}