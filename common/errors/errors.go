package errors

import (
	"fmt"
	"regexp"
)

type commandError struct {
	msg string
	userError bool
}

func (c commandError) Error() string {
	return c.msg
}

func (c commandError) IsUserError() bool {
	return c.userError
}

func UserError(a ...interface{}) commandError {
	return commandError{msg: fmt.Sprintln(a...), userError: true}
}

func SystemError(a ...interface{}) commandError {
	return commandError{msg: fmt.Sprintln(a...), userError: false}
}

func SystemErrorF(format string, a ...interface{}) commandError {
	return commandError{msg: fmt.Sprintf(format, a...), userError: false}
}

var userErrorRegexp = regexp.MustCompile("argument|flag|shorthand")

func IsUserError(err error) bool {
	if cErr, ok := err.(commandError); ok && cErr.IsUserError() {
		return true
	}

	return userErrorRegexp.MatchString(err.Error())
}