package guild

/*
guild -> raids -> parse (from rcl csv) <filename>  *parse should check to see if given item is already on user for that date.
*/

import (
	"fmt"

	"github.com/spf13/cobra"
	//jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	"gitlab.com/hierarchy-guild/lct/common/types/guild"
	
)

var (
	toJson bool
	toMD bool
)

func all(name string) *cobra.Command {
	allFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()

		toJ, err := cmd.Flags().GetBool("json")
		if err != nil {
			return err
		}

		toM, err := cmd.Flags().GetBool("md")
		if err != nil {
			return err
		}

		err = common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		guildFile := viper.GetString("guild")
		guild := guild.LoadGuild(guildFile)


		if toJ {
			s, err := common.PrettyPrint(guild)
			if err != nil {
				return err
			}

			fmt.Println(s)
			return nil
		}

		fmt.Println("Name: ", guild.Name)
		fmt.Println("Server: ", guild.Server)
		fmt.Println("Faction: ", guild.Faction)
		fmt.Println("----------------")
		for _, v := range guild.Players {
			fmt.Println()
			v.Display(toM)
			fmt.Println()
		}

		return nil
	}

	allCmd := cli.NewCommand("all", "Display the entire guild", "Display the entire guild", allFunc)
	allCmd.Flags().BoolVar(&toJson, "json", false, "Display output as json")
	allCmd.Flags().BoolVar(&toMD, "md", false, "Display output as markdown")
	return allCmd.GetCommand()
}