package guild

/*

guild -> load (pull from git)
guild -> update (push to git)
guild -> raids -> all
guild -> raids -> raid *no param gives last
guild -> raids -> raid <date>
guild -> raids -> parse (from rcl csv) <filename>  *parse should check to see if given item is already on user for that date.

*/

import (
	"github.com/spf13/cobra"

	"gitlab.com/hierarchy-guild/lct/actions/guild/raids"
	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
)

func GetCommand(name string) *cobra.Command {
	rootFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		return nil
	}

	rootCmd := cli.NewCommand("guild", "Interact with a guild", "Interact with a guild", rootFunc)
	rootCmd.AddCommand(all(name))
	rootCmd.AddCommand(load(name))
	rootCmd.AddCommand(update(name))
	rootCmd.AddCommand(raids.GetCommand(name))
	
	
	rootCmd.MinimumNArgs(1)

	return rootCmd.GetCommand()
}