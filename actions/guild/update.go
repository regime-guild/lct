package guild

/*
guild -> update (push to git)
*/

import (
	"fmt"
	"time"

	"github.com/spf13/cobra"
	//jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	
)


func update(name string) *cobra.Command {
	updateFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		tracking := viper.GetString("tracking")
		trackingPath, err := common.LoadRepo(tracking)
		if err != nil {
			return err
		}

		fmt.Println("Tracking at: " + trackingPath)

		gr, err := common.GetRepo(trackingPath)
		if err != nil {
			return fmt.Errorf("Failed to load items database (%s)", err)
		}

		if gr.IsClean() {
			common.CheckIfError(fmt.Errorf("No changes to items database to update remote database."))
		}

		guildFile := viper.GetString("guild")
		gr.Add(guildFile)

		msg := fmt.Sprintf("Updating %s on %s", guildFile, time.Now().Format(common.DATEFMT))
		gr.Commit(msg, time.Now())
		gr.Push()

		// Open MR

		return nil
	}

	updateCmd := cli.NewCommand("update", "Write a guild to a remote git data store", "Write a guild to a remote git data store", updateFunc)
	
	return updateCmd.GetCommand()
}