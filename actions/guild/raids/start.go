package raids

/*
guild -> raids -> start [date]
*/

import (
	"fmt"

	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	
)


func start(name string) *cobra.Command {
	getFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		jww.DEBUG.Printf("Args: %v", args)
		jww.DEBUG.Println("No Args, start a raid with current date (branch tracking)")
		jww.DEBUG.Println("Args, start a raid with the given date (branch tracking)")
	    jww.DEBUG.Println("return")

		return fmt.Errorf("This function is not yet implemented.\n")
	}

	getCmd := cli.NewCommand("get [date]", "Display a single raid", "Display a single raid", getFunc)
	
	return getCmd.GetCommand()
}