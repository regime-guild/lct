package raids

/*
guild -> raids -> end
*/

import (
	"fmt"

	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	
)


func end(name string) *cobra.Command {
	getFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		jww.DEBUG.Println("End current raid by adding all files to the branch and commiting")
	    jww.DEBUG.Println("return")

		return fmt.Errorf("This function is not yet implemented.\n")
	}

	getCmd := cli.NewCommand("get [date]", "Display a single raid", "Display a single raid", getFunc)
	
	return getCmd.GetCommand()
}