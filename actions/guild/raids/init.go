package raids

/*
guild -> raids -> all
guild -> raids -> get *no param gives last
guild -> raids -> get <date>
guild -> raids -> parse (from rcl csv) <filename>  *parse should check to see if given item is already on user for that date.
*/

import (
	"github.com/spf13/cobra"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"

	
)

func GetCommand(name string) *cobra.Command {
	rootFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		return nil
	}

	rootCmd := cli.NewCommand("raids", "Interact with guild raids", "Interact with guild raids", rootFunc)
	
	rootCmd.AddCommand(get(name))
	rootCmd.AddCommand(parse(name))
	rootCmd.MinimumNArgs(1)

	return rootCmd.GetCommand()
}