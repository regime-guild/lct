package raids

/*
guild -> raids -> parse (from rcl csv) <filename>  *parse should check to see if given item is already on user for that date.
*/

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	"gitlab.com/hierarchy-guild/lct/common/types/itemdb"
	"gitlab.com/hierarchy-guild/lct/common/types/loot"
	g "gitlab.com/hierarchy-guild/lct/common/types/guild"
	"gitlab.com/hierarchy-guild/lct/common/types/player"
	
)


func parse(name string) *cobra.Command {
	parseFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		jww.DEBUG.Printf("Args: %v", args)
		jww.DEBUG.Println("Parse a raid from RCLootCouncil CSV")
		jww.DEBUG.Println("return")
		
		guildFile := viper.GetString("guild")
		guild := g.LoadGuild(guildFile)

		items := itemdb.GetInstance()

		csvfile, err := os.Open(args[0])
		if err != nil {
			return err
		}

		r := csv.NewReader(csvfile)

		c := &g.Guild{}
		for {
			record, err := r.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				fmt.Println(err)
				break
			}

			// Filter out items that are not Need/Upgrade/BIS
			if !common.Contains(record[7], "Offspec") &&
				!common.Contains(record[7], "PVP") &&
				!common.Contains(record[7], "Offline") &&
				!common.Contains(record[7], "Pass") &&
				!common.Contains(record[7], "Awarded") &&
				!common.Contains(record[7], "Button4") &&
				!common.Contains(record[7], "wait") &&
				!common.Contains(record[7], "Autopass") &&
				!common.Contains(record[7], "Personal") &&
				!common.Contains(record[7], "Disenchant") {

				found := items.ID()[record[5]]
				pn := strings.Split(record[0], "-")[0]
				pc := strings.Title(strings.ToLower(record[9]))
				rd, _ := time.Parse(common.DATEFMT, strings.ReplaceAll(record[1], "/", "-"))

				jww.DEBUG.Printf("R: %s, %s, %s, %s, %s, %s", pn, pc, rd, found.Name, found.ID, record[7])

				gp := guild.GetPlayer(strings.Split(record[0], "-")[0])
				if gp == nil {
					gp = player.NewPlayer(pn, pc, strings.ToUpper("TBD"))
				}


				gp.AddLoot(loot.NewLoot(found,  rd))
				guild.UpdatePlayer(*gp)
				c.UpdatePlayer(*gp)
			}
		}
		jww.DEBUG.Printf("Items found in csv: %d", len(c.Players))

		for _, v := range c.Players {
			v.Display(false)
		}

		guild = guild.UpdateGuild(guildFile)
		
		return nil
	}

	parseCmd := cli.NewCommand("parse", "Parse a raid from RCLootCouncil CSV", "Parse a raid from RCLootCouncil CSV", parseFunc)
	
	return parseCmd.GetCommand()
}