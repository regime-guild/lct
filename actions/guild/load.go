package guild

/*
guild -> load (pull from git)
*/

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	
)


func load(name string) *cobra.Command {
	loadFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		tracking := viper.GetString("tracking")
		trackingPath, err := common.LoadRepo(tracking)
		common.CheckIfError(err)

		fmt.Println("Tracking at: " + trackingPath)

		gr, err := common.GetRepo(trackingPath)
		if err != nil {
			return fmt.Errorf("Failed to load guild tracking database (%s)", err)
		}

		gr.Pull()

		return nil
	}

	loadCmd := cli.NewCommand("load", "Load a guild from remote git data store", "Load a guild from remote git data store", loadFunc)
	
	return loadCmd.GetCommand()
}