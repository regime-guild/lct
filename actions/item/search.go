package item

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	"gitlab.com/hierarchy-guild/lct/common/types/itemdb"
)


func search(name string) *cobra.Command {
	searchFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}		

		jww.DEBUG.Printf("Arguments: %v", args)

		searchString := strings.Join(args, " ")

		items := itemdb.GetInstance()
		found := items.Search(searchString)
		
		if len(found) == 0 {
			return fmt.Errorf("Item: %s not found.\n", searchString)
		}		

		itemdb.Display(found)
		
		return nil
	}

	searchCmd := cli.NewCommand("search", "Search the ItemDB",
								"Search the ItemDB", searchFunc)
	searchCmd.MinimumNArgs(1)

	return searchCmd.GetCommand()
}
