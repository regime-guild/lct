package item

import (
	"fmt"

	"github.com/spf13/cobra"

	"gitlab.com/hierarchy-guild/lct/common/cli"
)


func stier(name string) *cobra.Command {
	stierFunc := func(cmd *cobra.Command, args []string) error {
		return fmt.Errorf("This function is not yet implemented.\n")
	}

	stierCmd := cli.NewCommand("stier", "List stier ItemDB",
								"List stier items from ItemDB", stierFunc)

	return stierCmd.GetCommand()
}