package item

/*
item -> load (pull from git)
*/

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	
)


func load(name string) *cobra.Command {
	loadFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		resources := viper.GetString("resources")
		resourcePath, err := common.LoadRepo(resources)
		if err != nil {
			return err
		}

		fmt.Println("Resources at: " + resourcePath)

		gr, err := common.GetRepo(resourcePath)
		if err != nil {
			return fmt.Errorf("Failed to load items database (%s)", err)
		}

		gr.Pull()

		return nil
	}

	loadCmd := cli.NewCommand("load", "Load a itemdb from remote git data store", "Load a itemdb from remote git data store", loadFunc)
	return loadCmd.GetCommand()
}