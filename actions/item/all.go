package item

import (
	"os"

	"github.com/jedib0t/go-pretty/table"
	"github.com/spf13/cobra"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	"gitlab.com/hierarchy-guild/lct/common/types/itemdb"
)

var (
	toJson bool
)

func all(name string) *cobra.Command {
	allFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()

		toJ, err := cmd.Flags().GetBool("json")
		if err != nil {
			return err
		}
		
		err = common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		items := itemdb.GetInstance()

		if toJ {
			common.PrettyPrint(items)
			return nil
		}

		t := table.NewWriter()
		t.SetOutputMirror(os.Stdout)
		t.AppendHeader(table.Row{"ID", "Item"})
		
		var trs []table.Row
		for _, v := range items.ID() {
			tr := table.Row{v.ID, v.Name}
			trs = append(trs, tr)
		}
		
		t.AppendRows(trs)
		t.AppendFooter(table.Row{"Count", len(items.Items)})
		t.Render()

		return nil
	}

	allCmd := cli.NewCommand("all", "Display all items in ItemDB", "Display all items in ItemDB", allFunc)
	allCmd.Flags().BoolVar(&toJson, "json", false, "Display output as json")
	allCmd.MinimumNArgs(0)

	return allCmd.GetCommand()
}
