package item

/*
guild -> update (push to git)
*/

import (
	"fmt"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	//jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	
)


func update(name string) *cobra.Command {
	updateFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		resources := viper.GetString("resources")
		resourcePath, err := common.LoadRepo(resources)
		if err != nil {
			return err
		}

		fmt.Println("Resources at: " + resourcePath)

		gr, err := common.GetRepo(resourcePath)
		if err != nil {
			return fmt.Errorf("Failed to load items database (%s)", err)
		}

		if gr.IsClean() {
			common.CheckIfError(fmt.Errorf("No changes to items database to update remote database."))
		}

		itemsFile := viper.GetString("items")
		gr.Add(resourcePath + "/" + itemsFile)

		msg := fmt.Sprintf("Updating %s on %s", itemsFile, time.Now().Format(common.DATEFMT))
		gr.Commit(msg, time.Now())
		gr.Push()

		// Open MR

		return nil
	}

	updateCmd := cli.NewCommand("update", "Write a itemdb to a remote git data store", "Write a itemdb to a remote git data store", updateFunc)
	
	return updateCmd.GetCommand()
}