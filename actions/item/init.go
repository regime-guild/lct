package item

import (
	"github.com/spf13/cobra"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
)

func GetCommand(name string) *cobra.Command {
	rootFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		return nil
	}

	rootCmd := cli.NewCommand("item", "Interace with the ItemDB",
							  "Interace with the ItemDB", rootFunc)
	rootCmd.AddCommand(all(name))
	rootCmd.AddCommand(load(name))
	rootCmd.AddCommand(search(name))
	rootCmd.AddCommand(stier(name))
	rootCmd.AddCommand(update(name))
	rootCmd.MinimumNArgs(1)

	return rootCmd.GetCommand()
}