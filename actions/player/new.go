package player

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	"gitlab.com/hierarchy-guild/lct/common/types/guild"
	"gitlab.com/hierarchy-guild/lct/common/types/player"
)


func new(name string) *cobra.Command {
	newFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()

		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		guildFile := viper.GetString("guild")
		guild := guild.LoadGuild(guildFile)

		if len(args) < 3 || len(args) > 3 {
			return fmt.Errorf("This function takes exactly 3 arguments")
		}

		gp := guild.GetPlayer(args[0])
		if gp != nil {
			return fmt.Errorf("Player: %s already exists.\n", gp.Name)
		}

		np := player.NewPlayer(strings.Title(args[0]), strings.Title(args[1]), strings.ToUpper(args[2]))

		np.Display(false)
		
		return nil
	}

	newCmd := cli.NewCommand("new", "Add a new player to the guild", "Add a new player to the guild", newFunc)
	newCmd.Flags().BoolVar(&toJson, "json", false, "Display output as json")
	newCmd.Flags().BoolVar(&toMD, "md", false, "Display output as markdown")
	newCmd.MinimumNArgs(1)

	return newCmd.GetCommand()
}
