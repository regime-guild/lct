package player

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	"gitlab.com/hierarchy-guild/lct/common/types/guild"
	"gitlab.com/hierarchy-guild/lct/common/types/player"
)


func compare(name string) *cobra.Command {
	compareFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()

		toM, err := cmd.Flags().GetBool("md")
		if err != nil {
			return err
		}

		err = common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		guildFile := viper.GetString("guild")
		guild := guild.LoadGuild(guildFile)

		var players []*player.Player
		
		for _, v := range args {
			player := guild.GetPlayer(v)
			if player == nil {
				return fmt.Errorf("Player: %s not found.\n", v)
			}
			players = append(players, player)
		}


		player.Compare(players, toM)

		
		return nil
	}

	compareCmd := cli.NewCommand("compare <player> <player> [...player]", "Compare given players", "Compare given players", compareFunc)
	compareCmd.Flags().BoolVar(&toMD, "md", false, "Display output as markdown")
	compareCmd.MinimumNArgs(2)

	return compareCmd.GetCommand()
}