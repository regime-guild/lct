package loot

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	
	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	"gitlab.com/hierarchy-guild/lct/common/types/guild"
	"gitlab.com/hierarchy-guild/lct/common/types/item"
	"gitlab.com/hierarchy-guild/lct/common/types/itemdb"
	"gitlab.com/hierarchy-guild/lct/common/types/loot"
	
)

var (
	date string
)

func getInput(found []item.Item) ([]item.Item, error) {
	fmt.Println()
	itemdb.Display(found)

	items := itemdb.GetInstance()

	var ia []item.Item
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Println()
		fmt.Print("Please enter the item ID of the item you would like to add: ")
		text, _ := reader.ReadString('\n')
		text = strings.Replace(text, "\n", "", -1)

		ia = items.Search(text)

		if (len(ia) == 1) {
			break
		}

		fmt.Println("ERROR: Item Not found or single ID not provided.")
	}
	return ia, nil
}


func add(name string) *cobra.Command {
	addFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		date := cmd.Flags().Lookup("date").Value.String()
		addDate, err := time.Parse(common.DATEFMT, date)

		guildFile := viper.GetString("guild")
		guild := guild.LoadGuild(guildFile)
		player := guild.GetPlayer(args[0])
		if player == nil {
			return fmt.Errorf("Player: %s not found.\n", args[0])
		}

		fmt.Println("Before:")
		player.Display(false)
		
		items := itemdb.GetInstance()

		var found []item.Item
		if( len(args) > 1 ) {
			searchString := strings.Join(args[1:], " ")
			found = items.Search(searchString)

			if (len(found) > 1) {
				found, err = getInput(found)
				if err != nil {
					return err
				}
			}
			player.AddLoot(loot.NewLoot(found[0], addDate))

			fmt.Println()
			fmt.Println("After:")
			player.Display(false)
		}

		guild.UpdatePlayer(*player)
		guild = guild.UpdateGuild(guildFile)
		return nil
	}

	addCmd := cli.NewCommand("add <player> [item]", "Add loot", "Add loot", addFunc)
	addCmd.Flags().StringVar(&date, "date", time.Now().Format(common.DATEFMT), "Set items file")
	addCmd.MinimumNArgs(1)

	return addCmd.GetCommand()
}