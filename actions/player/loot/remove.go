package loot

import (
	"fmt"

	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	
)


func remove(name string) *cobra.Command {
	removeFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		jww.DEBUG.Printf("Args: %v", args)
		jww.DEBUG.Println("Display Player")
		jww.DEBUG.Println("Prompt for loot item to remove.")
		jww.DEBUG.Println("Remove item")
		jww.DEBUG.Println("Display updated player")
	    jww.DEBUG.Println("return")

		return fmt.Errorf("This function is not yet implemented.\n")
	}

	removeCmd := cli.NewCommand("remove <player> [item number]", "Remove loot", "Remove loot", removeFunc)
	removeCmd.MinimumNArgs(1)
	return removeCmd.GetCommand()
}