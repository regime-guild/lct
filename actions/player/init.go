package player

/*
player -> manage -> add <name> <class> <spec>
                 -> delete <name>
player -> get <name>
player -> stier <name>
player -> loot -> add <name> <item> <date>
			   -> remove <name> <item>
player -> compare <name1> ... <nameN>
*/

import (
	"github.com/spf13/cobra"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	"gitlab.com/hierarchy-guild/lct/actions/player/loot"

	
	//"gitlab.com/hierarchy-guild/lct/common/types/item"
	//"gitlab.com/hierarchy-guild/lct/common/types/itemdb"
	//"gitlab.com/hierarchy-guild/lct/common/types/loot"
	//"gitlab.com/hierarchy-guild/lct/common/types/player"
	//"gitlab.com/hierarchy-guild/lct/common/types/guild"
)

func GetCommand(name string) *cobra.Command {
	rootFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()
		err := common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		return nil
	}

	rootCmd := cli.NewCommand("player", "Interace with the player list from the loaded guild", "Interace with the player list from the loaded guild", rootFunc)
	
	rootCmd.AddCommand(compare(name))
	rootCmd.AddCommand(get(name))
	rootCmd.AddCommand(new(name))
	rootCmd.AddCommand(stier(name))
	rootCmd.AddCommand(loot.GetCommand(name))
	rootCmd.MinimumNArgs(1)

	return rootCmd.GetCommand()
}