package player

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/hierarchy-guild/lct/common"
	"gitlab.com/hierarchy-guild/lct/common/cli"
	"gitlab.com/hierarchy-guild/lct/common/types/guild"
)

var (
	toJson bool
	toMD bool
)

func get(name string) *cobra.Command {
	getFunc := func(cmd *cobra.Command, args []string) error {
		config := cmd.Root().PersistentFlags().Lookup("config").Value.String()
		level := cmd.Root().PersistentFlags().Lookup("level").Value.String()

		toJ, err := cmd.Flags().GetBool("json")
		if err != nil {
			return err
		}

		toM, err := cmd.Flags().GetBool("md")
		if err != nil {
			return err
		}

		err = common.InitConfig(name, config, level)
		if err != nil {
			return err
		}

		guildFile := viper.GetString("guild")
		guild := guild.LoadGuild(guildFile)
		player := guild.GetPlayer(args[0])
		if player == nil {
			return fmt.Errorf("Player: %s not found.\n", args[0])
		}

		
		if toJ {
			s, err := player.ToJson()
			if err != nil {
				return err
			}

			fmt.Println(s)
			return nil
		}

		player.Display(toM)
		
		return nil
	}

	getCmd := cli.NewCommand("get", "Get a player from the loaded guild", "Get a player from the loaded guild", getFunc)
	getCmd.Flags().BoolVar(&toJson, "json", false, "Display output as json")
	getCmd.Flags().BoolVar(&toMD, "md", false, "Display output as markdown")
	getCmd.MinimumNArgs(1)

	return getCmd.GetCommand()
}
